use xtra::prelude::*;

#[spaad::entangled]
pub struct Printer {
    times: usize,
}

#[spaad::entangled]
impl Actor for Printer {}

#[spaad::entangled]
impl Printer {
    #[spaad::spawn]
    pub fn new() -> Self {
        Printer { times: 0 }
    }

    #[spaad::handler]
    pub fn print(&mut self, to_print: String) {
        self.times += 1;
        println!(
            "Printing {}. Printed {} times so far.",
            to_print, self.times
        );
    }
}
